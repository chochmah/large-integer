#pragma once

#include <windows.h>
#include <winnt.h>
#include <intrin.h>


inline NT_TIB* getTib()
{
	return (NT_TIB*)__readgsdword(0x18);
}
inline size_t get_allocated_stack_size()
{
	return (size_t)getTib()->StackBase - (size_t)getTib()->StackLimit;
}

void somewhere_in_your_thread()
{

	size_t sp_value = 0;
	_asm { mov[sp_value], esp }
	size_t used_stack_size = (size_t)getTib()->StackBase - sp_value;

	printf("Number of bytes on stack used by this thread: %u\n",
		used_stack_size);
	printf("Number of allocated bytes on stack for this thread : %u\n",
		get_allocated_stack_size());

}
