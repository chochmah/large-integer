#pragma once

#include <chrono>
#include <ctime>


//-----------------------------------------------------------------------------
namespace testSpace
{
	//-----------------------------------------------------------------------------

	template<typename intL>
	void ValidityTest(const uint64_t count_)
	{
		std::cout << "test";

		intL a;
		intL b;
		b = (uint64_t)87679121;
		for (size_t i = 0; i < 100; i++)
			a += b;

		for (size_t i = 0; i < 100; i++)
			a = a - (intL)"87679121";
		if (a != 0)
			std::cout << "Error!\n";


		for (size_t i = 0; i < count_; i++)
		{
			if ((i % (count_ / 10)) == 0)
				std::cout << ".";


			int64_t		int_x, int_y;			

			int_x = rand() - RAND_MAX / 2;
			int_y = rand() - RAND_MAX / 2;

			intL x(int_x);
			intL y(int_y);

			//-----------------------------------------------------------------------------
			// Test comparators

			if (((x == y) && !(int_x == int_y)) ||
				((x != y) && !(int_x != int_y)) ||
				((x < y) && !(int_x < int_y)) ||
				((x > y) && !(int_x > int_y)) ||
				((x <= y) && !(int_x <= int_y)) ||
				((x >= y) && !(int_x >= int_y))
				)
			{
				std::cout << (x < y) << " <> " << (int_x < int_y) << "\n";
			}

			//-----------------------------------------------------------------------------
			// Test operators

			if (((x + y) != (int_x + int_y)))
			{
				std::cout << "	" << int_x << " + " << int_y << "\n";
				std::cout << "	" << x << " + " << y << "\n";
				std::cout << (x + y) << "	!=	 " << (int_x + int_y) << " (+operator)\n";
			}

			if (((x - y) != (int_x - int_y)))
			{
				std::cout << "	" << x << " - " << y << "\n";
				std::cout << (x - y) << "	!=	 " << (int_x - int_y) << " (-operator)\n";
			}

			if (x * x - y * y != (x + y) * (x - y))
			{
				std::cout << "add/sub/mul mismatch\n";
			}

			if (y != 0)
			{
				auto m = x.div(y);
				auto t = std::get<0>(m) * y + std::get<1>(m);
				if (x != t) {
					std::cout << "Division/Multiplication mismatch\n";
					std::cout << "	" << x << " // " << y << "\n";
					std::cout << "  " << std::get<0>(m) << " * " << y << " + " << std::get<1>(m) << " = " << t << "\n";
					std::get<0>(m).printDecimal();
					std::get<1>(m).printDecimal();
				}


				if (((x * y) != (int_x * int_y)))
				{
					std::cout << x << " * " << y << "\n";
					std::cout << (x * y) << "	!=	 " << (int_x * int_y) << " (*operator)\n";
				}
			}

			{
				intL k = 1;
				for (int i = 0; i < 20; i++)
					k *= x*y;

				intL q = k;
				for (int i = 0; i < 20; i++)
				{
					auto m = q.div(x*y);
					if (std::get<1>(m) != 0)
						std::cout << "Division Error (2)\n";
					q = std::get<0>(m);
				}

				if (q != 1)
					std::cout << "Division/Multiplication mismatch (1)\n";
			}


			//-----------------------------------------------------------------------------
			// shift operators

			int shift = rand() % 20;
			
			
			if (x << shift != x * intL::pow(intL(2),shift))
			{
				std::cout << x << " << " << shift <<"\n";
				std::cout << (x << shift) << "	!=	 " << x * intL::pow(2, shift) << " (<<operator/pow-operator)\n";
			}
		
			if (((x << shift) >> shift) != x)
			{
				std::cout << "(" << x << " << " << shift << ") >> " << shift << "\n";
				std::cout << ((x << shift) >> shift) << "	!=	 " << x << " (<<operator/>>operator)\n";
			}
		}

		std::cout << "finished\n";
	}


	//-----------------------------------------------------------------------------
	template<typename Functor, typename intL>
	uint64_t timeFunction(
							Functor functor, 
							intL x, 
							const uint64_t _cycles, 
							const std::string _op)
	{
		int result = 0;

		auto start = std::chrono::system_clock::now();
		
		for (int i = 0; i < _cycles; i++)
			result += functor(i);

		auto end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = end - start;
		std::time_t end_time = std::chrono::system_clock::to_time_t(end);
		
		std::cout << _op << ": " << elapsed_seconds.count() << "s	x" << _cycles << "\n";

		return result;
	}


	//-----------------------------------------------------------------------------
	// run _cylcles operations with samplesize numbers

	template<typename intL>
	void PerformanceTest(const uint64_t samplesize, const uint64_t _cycles)
	{
		srand(42);

		//~1000 digit prime (yes, this is a prime :) )		
		intL lnum = "9444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444448484498999";
		//lnum = intL::pow(lnum,8);

		intL lnumb = "262641725682127839334668938847190331798311145333616792958372838424857156775007227102454928148183167823211534641943299795962309579874682913568608650959193370824474863282145037585560618568778906679404993323367715662527874977471424467472818616772123866421714232029284828476022113987781717658486369598393166193032285497153837454766280540159";		
		//lnumb = intL::pow(lnumb, 8);

		intL::setDisplayDigits(1);
		std::cout << "processing operations on numbers < " << lnum << "\n";
		intL::setDisplayDigits(10);

		std::cout << "generating random numbers...\n";

		std::vector<intL>		buffer(samplesize);
		std::vector<intL>		divbuffer(samplesize);
		std::vector<uint64_t>	mapper1(_cycles);
		std::vector<uint64_t>	mapper2(_cycles);

		{
			auto lambda = [&buffer, &divbuffer, lnum, lnumb](int i)
			{ 
				buffer[i] = intL::rand(lnum) - lnum / (intL)2;
				divbuffer[i] = intL::rand(lnumb) - lnumb / (intL)2;
				return 1;
			};
			timeFunction(lambda, intL(0), samplesize, "rnd"); //pass lambda
		}


		for (uint64_t i = 0; i < _cycles; i++)
		{
			mapper1[i] = rand() % samplesize;
			mapper2[i] = rand() % samplesize;
		}

		uint64_t test = 0;

		{
			auto lambda = [&buffer, &mapper1, &mapper2](int i)
			{ return (buffer[mapper1[i]] + buffer[mapper2[i]]).getSign(); };
			test += timeFunction(lambda, intL(0), _cycles,"add"); //pass lambda
		}
		{
			auto lambda = [&buffer, &mapper1, &mapper2](int i)
			{ return (buffer[mapper1[i]] - buffer[mapper2[i]]).getSign(); };
			test += timeFunction(lambda, intL(0), _cycles,"sub"); //pass lambda
		}
		{
			auto lambda = [&buffer, &mapper1, &mapper2](int i)
			{ return (buffer[mapper1[i]] * buffer[mapper2[i]]).getSign(); };
			test += timeFunction(lambda, intL(0), _cycles / 100, "mul"); //pass lambda
		}
		{
			auto lambda = [&buffer, &mapper1, &mapper2, &divbuffer](int i)
			{ return (buffer[mapper1[i]] / divbuffer[mapper2[i]]).getSign(); };
			test += timeFunction(lambda, intL(0), _cycles / 1000, "div"); //pass lambda
		}	
	
		//saveguard against compiler optimization
		if (test<0) std::cout << test << "\n"; 	
	}


}