//-----------------------------------------------------------------------------
//
//	Explicit template instantiation for bool
//
//-----------------------------------------------------------------------------

#include "LargeIntegerClass.h"


//-----------------------------------------------------------------------------
template <>
intLarge<bool>& intLarge<bool>::operator=(const int64_t val_)
{
	uint64_t val = std::abs(val_);
	sign = (val_ < 0) ? NEGATIV : POSITIV;
	data.clear();

	uint64_t width = (val == 0) ? 0 : (uint64_t)floor(log2(val));
	data.resize(width + (uint64_t)1);

	for (int64_t i = width; i >= 0; i--)
	{
		uint64_t v = (uint64_t)std::pow(2, i);
		if (val >= v)
		{
			val -= v;
			data[i] = true;
		}
		else
			data[i] = false;
	}

	return *this;
}


//-----------------------------------------------------------------------------
template <>
intLarge<bool> intLarge<bool>::operator*(const intLarge<bool> &val_) const
{
	intLarge<bool> result(0);

	if ((this->data.size() + val_.data.size()) * typesize_bits > bitmax - 1)
	{
		std::cout << "intLarge<type> overflow\n";
		return result;
	}

	result.data.resize(this->data.size() + val_.data.size());

	int64_t counter{ 0 };

	for (uint64_t o : this->data)
	{
		if (o)
		{
			bool overflow = 0;

			for (int i = 0; i < val_.data.size(); i++)
			{
				if (overflow)
				{
					if (val_.data[i] && result.data[i + counter])
					{
						result.data[i + counter] = true; // .push_back(1);
					}
					else
					{
						if (val_.data[i] || result.data[i + counter])
						{
							result.data[i + counter] = false; //.push_back(0);
						}
						else
						{
							result.data[i + counter] = true; //push_back(1);
							overflow = 0;
						}
					}
				}
				else
				{
					if (val_.data[i] && result.data[i + counter])
					{
						result.data[i + counter] = false; //push_back(0);
						overflow = 1;
					}
					else
					{
						if (val_.data[i] || result.data[i + counter])
						{
							result.data[i + counter] = true; //push_back(1);
						}
						else
						{
							result.data[i + counter] = false; //push_back(0);
						}
					}
				}
			}

			if (overflow)
			{
				result.data[val_.data.size() + counter] = true;
			}

		}

		counter++;
	}

	result.RemoveLeadingZeros();
	result.sign = (result.isZero()) ? POSITIV : (this->sign == val_.sign) ? POSITIV : NEGATIV;

	return result;
}


//-----------------------------------------------------------------------------
template <>
intLarge<bool> &intLarge<bool>::AddAbsoluteOrdered(
													const intLarge<bool> * small,
													const intLarge<bool> * big,
													intLarge<bool> &result)
{
	// add from 0 to small->data.size		
	bool overflow = 0;

	result.data.resize(big->data.size());

	for (size_t i = 0; i < small->data.size(); i++)
	{
		if (overflow)
		{
			if (small->data[i] && big->data[i])
				result.data[i] = true; // .push_back(1);
			else
			{
				if (small->data[i] || big->data[i])
					result.data[i] = false; //.push_back(0);
				else
				{
					result.data[i] = true; //push_back(1);
					overflow = 0;
				}
			}
		}
		else
		{
			if (small->data[i] && big->data[i])
			{
				result.data[i] = false; //push_back(0);
				overflow = 1;
			}
			else
			{
				if (small->data[i] || big->data[i])
					result.data[i] = true; //push_back(1);
				else
					result.data[i] = false; //push_back(0);
			}
		}
	}

	//add overflow and copy remaining big->data
	for (size_t i = small->data.size(); i < big->data.size(); i++)
	{
		bool currentbit = (big->data[i] ^ overflow);
		result.data[i] = currentbit; 

		bool andbit(big->data[i] & overflow);
		overflow = (andbit) ? 1 : 0;
	}

	if (overflow)
	{
		if (result.data.size() * typesize_bits > bitmax - 1)
		{
			std::cout << "intLarge<type> overflow\n";
			result = (uint64_t)0;
		}
		else
			result.data.push_back(1);
	}

	return result;
}


//-----------------------------------------------------------------------------
// returns {this divided by val_ , remainder}

template <>
std::tuple<intLarge<bool>, intLarge<bool>> intLarge<bool>::div(const intLarge<bool> & val_) const
{
	if (val_.isZero())
		FatalError("division by zero");

	intLarge<bool> result;
	result.data.clear();

	const intLarge<bool>	&divisor = val_;
	intLarge<bool>			dividend = *this;
	uint64_t				divisor_size = val_.data.size();
	uint64_t				dividend_size = this->data.size();

	if (dividend_size < divisor_size)
		return std::make_tuple(intLarge(), *this);

	intLarge<bool> r;
	r.data.resize(divisor_size - 1, 0);
	for (int k = 0; k < divisor_size - 1; k++)
		r.data[divisor_size - k - 2] = dividend.data[dividend_size - k - 1];

	for (int64_t i = dividend_size - divisor_size; i >= 0; i--)
	{

		if (r.isZero())
			r.data[0] = dividend.data[i];
		else
			r.data.insert(r.data.begin(), 1, dividend.data[i]);

		//check if divident is smaller than current power		
		if (compareAbsolute(r, divisor) != COMP::LE)
		{
			//fits in
			SubAbsoluteOrdered(&divisor, &r, r);
			result.data.push_back(true);
		}
		else
		{
			//does not fit in
			result.data.push_back(false);
		}
	}

	// reverse number
	for (int i = 0; i < result.data.size() / 2; i++)
		std::swap(result.data[i], result.data[result.data.size() - i - 1]);

	result.RemoveLeadingZeros();

	if (result.data.size() == 0) result.data.push_back(false);
	result.sign = (this->sign == val_.sign) ? POSITIV : NEGATIV;
	r.sign = (r==0) ? POSITIV : this->sign;

	return std::make_tuple(result, r);
}


//-----------------------------------------------------------------------------
template <>
std::vector<bool> intLarge<bool>::AbsoluteConvertToVectorBool() const
{
	return data;
}


//-----------------------------------------------------------------------------
template <>
std::vector<int8_t> intLarge<bool>::AbsoluteConvertToVectorDigits() const
{
	std::vector<int8_t> dec;
	std::vector<int8_t> pot;

	for (uint64_t i = 0; i < this->data.size(); i++)
	{
		for (uint64_t u = 0; u < typesize_bits; u++)
		{
			if (i == 0 && u == 0)
				pot.push_back(1);
			else
			{
				//double pot
				int8_t overflow{ 0 };

				for (uint64_t k = 0; k < pot.size(); k++)
				{
					pot[k] *= 2;
					pot[k] += overflow;
					if (pot[k] > 9)
					{
						overflow = (pot[k] - (pot[k] % 10)) / 10;
						pot[k] = (pot[k] % 10);
					}
					else
						overflow = 0;
				}
				if (overflow)
					pot.push_back(overflow);
			}

			if (this->data[i])
			{
				// add current pot to number dec
				int8_t overflow{ 0 };

				if (dec.size() < pot.size()) dec.resize(pot.size());

				for (uint64_t k = 0; k < pot.size(); k++)
				{
					dec[k] += pot[k] + overflow;
					if (dec[k] > 9)
					{
						overflow = (dec[k] - (dec[k] % 10)) / 10;
						dec[k] = (dec[k] % 10);
					}
					else
						overflow = 0;
				}
				if (overflow)
					dec.push_back(overflow);
			}
		}
	}

	return dec;
}


//-----------------------------------------------------------------------------
template<>
void intLarge<bool>::flipBits(intLarge<bool> &val_)
{
	for (auto &o : val_.data)
		o = o!=true;
}


//-----------------------------------------------------------------------------
template <>
bool intLarge<bool>::getBit(const bool _var, const uint64_t _p)
{
	return _var;
}


//-----------------------------------------------------------------------------
// absolutely brilliant random-method, where is my fields-medal? 

template <class type>
intLarge<type> intLarge<type>::rand(const intLarge<type> &n_)
{
	if (n_ < 1) return intLarge(0);

	intLarge<type> result;
	result.data.resize(n_.data.size());

	do {
		for (int64_t i = n_.data.size() - 1; i >= 0; i--)
			result.data[i] = (std::rand() % 2 == 1);
	} while (result > n_ - (intLarge)1);

	return result;
}


//-----------------------------------------------------------------------------
template <>
intLarge<bool>& intLarge<bool>::operator++()
{
	intLarge<bool> x = 1;

	*this += x;
	return *this;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
template class intLarge<bool>;