//-----------------------------------------------------------------------------
//	Dynamic Size Large Integer Class
//
//	Not very fast but does the job
//-----------------------------------------------------------------------------

#pragma once

#include <iostream>  
#include <tuple>
#include <bitset>
#include <vector>
#include <type_traits>

//-----------------------------------------------------------------------------
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


//-----------------------------------------------------------------------------
// Type for comparisions (less, greater, etc.)
enum class COMP { LE, GE, LEQ, GEQ, EQ };


//-----------------------------------------------------------------------------
template <class type>
class intLarge
{
public: 
	enum SIGN { NEGATIV, POSITIV };		

private:	
	static uint64_t bitmax;
	static uint64_t displayDigits;

	SIGN sign;			//	0	negative
						//	1	positive
	std::vector<type> data;

	static const uint64_t		typesize_bits = 
									(std::is_same<type, bool>::value) ? 1 : sizeof(type) * 8;

	COMP						compare(const intLarge<type>& rhs) const;
	static COMP					compareAbsolute(
														const intLarge<type>& rhs, 
														const intLarge<type>& lrs);
	static COMP 				compareAbsoluteHelper(
														const intLarge<type>& rhs, 
														const intLarge<type>& lrs);
	
	bool						OrderArgumentsAbsolute(
														const intLarge<type> **lhs, 
														const intLarge<type> **rhs) const;
	inline bool					isZero() const;
	void						FlipSign() { sign = (sign == POSITIV) ? NEGATIV : POSITIV; };

	static intLarge<type> 		&AddAbsoluteOrdered(
														const intLarge<type> *lhs,
														const intLarge<type> *rhs,
														intLarge<type> &result) 
														{ return result; };
	static intLarge<type> 		&SubAbsoluteOrdered(
														const intLarge<type> *lhs,
														const intLarge<type> *rhs,
														intLarge<type> &result);

	static void					FatalError(const std::string& _text);	

	static bool					getBit(const type _var, const uint64_t _p);

public:
	const uint64_t				getFirstBitPosition() const { return 0; };
	void						RemoveLeadingZeros();
	void						setSign(const SIGN sign_) { sign = sign_; };
	bool						getSign() const { return (sign==POSITIV); };
	void						addDigit(bool digit) { data.push_back(digit); }
	void						setBit(uint64_t p);
	std::vector<int8_t>			AbsoluteConvertToVectorDigits() const { std::vector<int8_t> a; return a; };
	std::vector<bool>			AbsoluteConvertToVectorBool()	const { std::vector<bool> a; return a; };

	// COMP::GEneral static functions
	
	static void				setBitSize(const uint64_t _size) { bitmax = _size; };
	static void				setDisplayDigits(const uint64_t _size)	 { displayDigits = _size; };
	static const uint64_t	getDisplayDigits() { return displayDigits; };
	static const uint64_t	getBitSize()	{ return bitmax; };	
	static inline void		flipBits(intLarge<type> &val_);

	// constructors / destructor

	intLarge<type>();
	intLarge<type>(const int64_t _x)			{ *this = _x; };
	//intLarge<type>(const int _x)				{ *this = (int64_t)_x; };	
	intLarge<type>(const std::string &val_);

	~intLarge<type>() { data.clear(); }

	// assignment operators

	intLarge<type>&					operator=	(const int64_t val_)	{ return *this; };
	intLarge<type>&					operator=	(const uint64_t val_)	{ return *this; };
	explicit operator 				int64_t		() const;			
	
	intLarge<type>&	operator+=	(intLarge<type>& rhs) { *this = *this + rhs; return *this; };
	intLarge<type>&	operator-=	(intLarge<type>& rhs) { *this = *this - rhs; return *this; };
	intLarge<type>&	operator*=	(intLarge<type>& rhs) { *this = *this * rhs; return *this; };
	intLarge<type>&	operator/=	(intLarge<type>& rhs) { *this = *this / rhs; return *this; };
	intLarge<type>&	operator%=	(intLarge<type>& rhs) { *this = *this % rhs; return *this; };

	// bitwise operators (shift)

	intLarge<type>	operator<<	(const uint64_t shift_) const { auto x = *this; return x <<= shift_;}
	intLarge<type>	operator>>  (const uint64_t shift_) const { auto x = *this; return x >>= shift_;}
	intLarge<type>&	operator<<=	(const uint64_t shift_);
	intLarge<type>&	operator>>=	(const uint64_t shift_);

	// standard arithmetic operators

	intLarge<type>	operator+	(const intLarge<type> &val_) const;
	intLarge<type>	operator-	(const intLarge<type> &val_) const;
	intLarge<type>	operator*	(const intLarge<type> &val_) const;
	intLarge<type>	operator/	(const intLarge<type> &val_) const { return std::get<0>(div(val_)); };

	intLarge<type>&	operator++	();
	intLarge<type>&	operator--	();
	intLarge<type>	operator++	(int) { return (*this)++; };
	intLarge<type>	operator--	(int) { return (*this)--; };

	std::tuple<intLarge<type>, intLarge<type>> div(const intLarge<type> &val_) const;

	// advanced artithmetic operators

	static		intLarge<type>		pow			(intLarge<type> base, intLarge<type> exp);
	static		intLarge<type>		root		(const intLarge<type> &z_);
	static		intLarge<type>		rand		(const intLarge<type> &n_);
	static		intLarge<type>		faculty		(const intLarge<type> &n_);
	static		intLarge<type>		abs			(const intLarge<type> &val_);
	static		intLarge<type>		digitSum	(const intLarge<type> &val_);
	static		intLarge<type>		mod			(const intLarge<type>& a, const intLarge<type>& b)
													{ return std::get<1>(a.div(b)); };
				intLarge<type>		operator%	(const intLarge<type>& val_) const
													{ return mod(*this, val_); };

	type		getSingleDigit(size_t p)  const;
	uint64_t	getBinaryDigits()	const	{ return getFirstBitPosition(); };
	uint64_t	getDecimalDigits()	const	{ return AbsoluteConvertToVectorDigits().size(); };		

	// relational operators

	bool		operator<	(const intLarge<type>& rhs) const;
	bool		operator>	(const intLarge<type>& rhs) const;
	bool		operator<=	(const intLarge<type>& rhs) const;
	bool		operator>=	(const intLarge<type>& rhs) const;
	bool		operator==	(const intLarge<type>& rhs) const;	
	bool		operator!=	(const intLarge<type>& rhs) const;

	// output methods
	
	void						printBinary() const;
	void						printDecimal() { std::cout << *this << std::endl; }

	//-----------------------------------------------------------------------------
	//template <class type>
	friend	std::ostream& operator<<(std::ostream& os, intLarge<type>& dt)
	{
		std::vector<int8_t>	dec = dt.AbsoluteConvertToVectorDigits();

		if (!dt.sign) os << "-";

		if (dec.size() == 0) dec.push_back(0);

		for (int64_t i = dec.size() - 1; 
				i >= MAX(0, (int64_t)dec.size() - (int64_t)intLarge::displayDigits); i--)
			os << (int)dec[i];

		if (dec.size() > intLarge::displayDigits)		
			os << "*10^" << (dec.size() - intLarge::displayDigits);

		return os;
	}	
};


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
template <class type>	uint64_t intLarge<type>::bitmax = 1024 * 1024 * 1024;
template <class type>	uint64_t intLarge<type>::displayDigits = 20;


//-----------------------------------------------------------------------------
template <class type>
COMP intLarge<type>::compareAbsoluteHelper(
	const intLarge<type>& rhs,
	const intLarge<type>& lrs)
{
	if (rhs.data.size() < lrs.data.size())
		return COMP::LE;
	if (rhs.data.size() > lrs.data.size())
		return COMP::GE;

	for (int64_t i = rhs.data.size() - 1; i >= 0; i--)
	{
		if (lrs.data[i] > rhs.data[i])
			return COMP::LE;
		if (lrs.data[i] < rhs.data[i])
			return COMP::GE;
	}

	return COMP::EQ;
}


//-----------------------------------------------------------------------------
//	compare absolute values |rhs| = a and |lhs| = b, return 
//	0	if a < b
//	1	if a >= b

template <class type>
COMP intLarge<type>::compareAbsolute(const intLarge<type>& rhs, const intLarge<type>& lrs)
{
	if (compareAbsoluteHelper(rhs, lrs) == COMP::LE) return COMP::LE;
	return COMP::GE;
}


//-----------------------------------------------------------------------------
//	compare this = a and rhs = b, return 
//	0	if a < b
//	1	if a > b
//	2	if a = b

template <class type>
COMP intLarge<type>::compare(const intLarge<type>& rhs) const
{
	if (!this->sign && rhs.sign)
		return COMP::GE;
	if (this->sign && !rhs.sign)
		return COMP::LE;

	bool retval = (this->sign) ? true : false;

	COMP temp = compareAbsoluteHelper(rhs, *this);

	if (temp == COMP::EQ)
		return COMP::EQ;
	else
	{
		if (this->sign == POSITIV)
		{
			if (temp != COMP::LE) return COMP::GE;
			else return COMP::LE;
		}
		else
		{
			if (!(temp != COMP::LE)) return COMP::GE;
			else return COMP::LE;
		}
	}
}


//-----------------------------------------------------------------------------
template <class type>
void intLarge<type>::FatalError(const std::string& _text)
{
	std::cout << "intLarge<type> fatal error: " << _text.c_str() << "\n";
	exit(1);
}


//-----------------------------------------------------------------------------
template <class type>
void intLarge<type>::RemoveLeadingZeros()
{
	if (data.size() == 0)
		FatalError("RemoveLeadingZeros - number has zero COMP::LEngth");

	uint64_t count = 0;
	while (this->data[this->data.size() - 1 - count] == false && count < data.size() - 1)
		count++;

	this->data.erase(this->data.end() - count, this->data.end());
}


//-----------------------------------------------------------------------------
template <class type>
intLarge<type>::operator int64_t() const
{
	if (this->data.size() >= 63)
	{
		std::cout << "number to large for conversation to 64bit\n";
		return 0;
	}

	int64_t r = 0;
	for (size_t i = 0; i < data.size(); i++)
		r += (int64_t)(data[i] * std::pow(2, i));

	if (!sign) r *= -1;

	return r;
};


//-----------------------------------------------------------------------------
template <class type>
intLarge<type>::intLarge()
{
	sign = POSITIV;
	data.push_back(0);
}


//-----------------------------------------------------------------------------
template <class type>
intLarge<type>::intLarge(const std::string &val_)
{
	*this = (uint64_t)0;
	this->sign = POSITIV;

	int64_t shift{ 0 };

	// check for negative sign	
	if (val_[0] == '-')
		shift++;

	intLarge<type> base_multiplier = 1;

	for (int64_t i = val_.size() - 1; i > shift - 1; i--)
	{
		if (val_[i] < 48 || val_[i]>57)
		{
			std::cout << "intLarge parsing error: not a number\n";
			*this = (uint64_t)0;
			return;
		}

		uint8_t		digit = val_[i] - 48;
		intLarge	d = digit;

		*this += d * base_multiplier;

		base_multiplier *= (intLarge)10;
	}

	if (shift)
		this->sign = NEGATIV;
}


//-----------------------------------------------------------------------------
template <class type>
void intLarge<type>::printBinary() const
{
	if (!sign) std::cout << "-";
	for (int i = (int)data.size() - 1; i >= 0; i--)
	{
		for (uint64_t u = 0; u < typesize_bits; u++)
			std::cout << getBit(data[i], u);
		std::cout << "\n";
	}
	std::cout << "\n";
}


//-----------------------------------------------------------------------------
template <class type>
bool intLarge<type>::isZero() const
{
	if (this->data.size() == 1 && this->data[0] == 0)
		return true;
	return false;
}


//-----------------------------------------------------------------------------
template <class type>
bool intLarge<type>::operator<(const intLarge<type>& rhs) const
{
	if (compare(rhs) == COMP::GE) return true;
	return false;
}


//-----------------------------------------------------------------------------
template <class type>
bool intLarge<type>::operator>(const intLarge<type>& rhs) const
{
	if (compare(rhs) == COMP::LE) return true;
	return false;
}


//-----------------------------------------------------------------------------
template <class type>
bool intLarge<type>::operator<=(const intLarge<type>& rhs) const
{
	if (compare(rhs) != COMP::LE) return true;
	return false;
}


//-----------------------------------------------------------------------------
template <class type>
bool intLarge<type>::operator>=(const intLarge<type>& rhs) const
{
	if (compare(rhs) == COMP::GE) return false;
	return true;
}

//-----------------------------------------------------------------------------
template <class type>
bool intLarge<type>::operator==(const intLarge<type>& rhs) const
{
	if (compare(rhs) == COMP::EQ) return true;
	return false;
}


//-----------------------------------------------------------------------------
template <class type>
bool intLarge<type>::operator!=(const intLarge<type>& rhs) const
{
	return !(*this == rhs);
}


//-----------------------------------------------------------------------------
template <class type>
intLarge<type> intLarge<type>::abs(const intLarge<type> &val_)
{
	intLarge<type> r = val_;
	r.sign = POSITIV;

	return r;
}


//-----------------------------------------------------------------------------
// returns the sum of decimal digits

template <class type>
intLarge<type> intLarge<type>::digitSum(const intLarge<type> & val_)
{
	intLarge<type> result;

	auto digits = val_.AbsoluteConvertToVectorDigits();
	for each (auto d in digits)
		result += (intLarge)d;

	return result;
}


//-----------------------------------------------------------------------------
// leftshifts the contend of data by n and fills from the right side with zeros

template <class type>
intLarge<type>& intLarge<type>::operator<<= (const uint64_t shift_)
{
	if (this->isZero())
		return *this;

	uint64_t	realshift		= shift_ % typesize_bits;
	uint64_t	blockshift		= (shift_ - realshift) / typesize_bits;	

	uint64_t n = (data.size() * typesize_bits < shift_) ? data.size() * typesize_bits : shift_;

	//shift bits	
	type mask_1 = 0; //UINT64_MAX - ((uint64_t)std::pow(2, (realshift)) - 1);

	for (int i = 0; i < realshift; i++)
		mask_1 |= (type)1 << (typesize_bits-i-1);
	
	// find first set bit in highest data block
	type		last_block	= data[data.size() - 1];
	int			headroom	= typesize_bits-1;
	
	while (last_block >>= 1)
		headroom--;

	// add blocks at the base
	if (blockshift>0)
		data.insert(data.begin(), blockshift, 0);

	// not enough space in first block, add new block
	if (headroom < realshift)
		data.resize(data.size() + 1);
	else
		data[data.size()-1] = data[data.size() - 1] << realshift;


	for (int64_t i = data.size() - 2; i >=0 ; i--)
	{
		type bring_over = (data[i] & mask_1);
		bring_over >>= typesize_bits - realshift;
		data[i + 1] = data[i + 1] + bring_over;
		data[i] = data[i] << realshift;
	}

	return *this;
}


//-----------------------------------------------------------------------------
// rightshifts the contend of data by n

template <class type>
intLarge<type>& intLarge<type>::operator>>= (const uint64_t shift_)
{
	uint64_t realshift = shift_ % typesize_bits;
	uint64_t blockshift = (shift_ - realshift) / typesize_bits;

	uint64_t n = (data.size() * typesize_bits < shift_) ? data.size() * typesize_bits : shift_;

	// delete blocks
	if (blockshift > 0)
	{
		data.erase(data.begin(), data.begin() + MIN(blockshift,data.size()));

		if (data.size() == 0)
		{
			data.push_back(0);
			sign = SIGN::POSITIV;
			return *this;
		}
	}

	//shift bits	
	//type  mask_1 = (uint64_t)std::pow(2, (realshift)) - 1;	
	
	type mask_1 = 0; //UINT64_MAX - ((uint64_t)std::pow(2, (realshift)) - 1);

	for (int i = 0; i < realshift; i++)
		mask_1 |= 1UL << i;

	for (int i = 0; i < data.size() - 1; i++)
	{
		data[i] = data[i] >> realshift;

		type bring_over = (data[i + 1] & mask_1);
		bring_over <<= typesize_bits - realshift;

		data[i] = data[i] + bring_over;
	}

	if (data.size() > 0)
	{
		data[data.size() - 1] = data[data.size() - 1] >> realshift;
		RemoveLeadingZeros();
	}
	else
	{
		data.push_back(0);
		sign = SIGN::POSITIV;
	}

	return *this;
}


//-----------------------------------------------------------------------------
template <class type>
bool intLarge<type>::OrderArgumentsAbsolute(const intLarge<type> **lhs,
	const intLarge<type> **rhs) const
{
	if (!lhs || !rhs)
		FatalError("intLarge::OrderArguments Nullpointer");

	const intLarge<type> *_lhs = *lhs;
	const intLarge<type> *_rhs = *rhs;

	// determine which binary has COMP::LEss digits and have lhs point to it			

	if (_lhs->getBinaryDigits() < _rhs->getBinaryDigits())
	{
		*lhs = _lhs;
		*rhs = _rhs;
	}
	else
	{
		*lhs = _rhs;
		*rhs = _lhs;
	}

	// if same size put smaller number into lhs
	if (_lhs->data.size() == _rhs->data.size())
	{
		if (compareAbsolute(*_rhs, *_lhs) == COMP::LE)
		{
			*lhs = _rhs;
			*rhs = _lhs;
		}
		else
		{
			*lhs = _lhs;
			*rhs = _rhs;
		}
	}

	return true;
}


//-----------------------------------------------------------------------------
// subtract abs() smaller number from abs() larger number
// using the Complement Method

template <class type>
intLarge<type> &intLarge<type>::SubAbsoluteOrdered(
													const intLarge<type> * small,
													const intLarge<type> * big,
													intLarge<type> &result)
{
	if (small->data.size() == 1 && !small->data[0])
	{
		result = *big;
		return result;
	}

	intLarge<type> temp;

	// fill second number with COMP::LEading zeros to match first number-length 
	// and copy to temp

	int64_t fill = big->data.size() - small->data.size();
	temp.data = small->data;

	if (fill!=0) 
		temp.data.insert(temp.data.end(), fill, false);

	// flip digits in second term
	flipBits(temp);
	
	// add 1 to temp
	++temp;

	// add big and temp, remove first digit from solution
	AddAbsoluteOrdered(&temp, big, result);
	
	if (result.data.size()>0)
		result.data.pop_back();

	result.RemoveLeadingZeros();

	return result;
}


//-----------------------------------------------------------------------------
template <class type>
intLarge<type> intLarge<type>::operator-(const intLarge<type> &val_) const
{
	intLarge	result;
	result.data.clear();

	const intLarge	*lhs = &val_;
	const intLarge	*rhs = this;

	COMP flipflag = compareAbsolute(*lhs, *rhs);

	result.sign = rhs->sign;

	OrderArgumentsAbsolute(&lhs, &rhs);

	if (lhs->sign == rhs->sign)
	{
		SubAbsoluteOrdered(lhs, rhs, result);
		if (flipflag == COMP::GE)
			result.FlipSign();
	}
	else
		AddAbsoluteOrdered(lhs, rhs, result);

	if (result.isZero()) result.sign = POSITIV;	

	return result;
}


//-----------------------------------------------------------------------------
template <class type>
intLarge<type>& intLarge<type>::operator--()
{
	*this -= (intLarge)1;
	return *this;
}


//-----------------------------------------------------------------------------
template <class type>
intLarge<type> intLarge<type>::operator+(const intLarge<type> &val_) const
{
	auto u = val_.isZero();

	if (val_.isZero()) return *this;
	if (this->isZero()) return val_;

	intLarge<type>	result;
	result.data.clear();

	const intLarge<type>	*lhs = this;
	const intLarge<type>	*rhs = &val_;

	result.sign = lhs->sign;

	OrderArgumentsAbsolute(&lhs, &rhs);

	if (lhs->sign == rhs->sign)
		AddAbsoluteOrdered(lhs, rhs, result);
	else
	{
		SubAbsoluteOrdered(lhs, rhs, result);
		if (lhs != &val_)
			result.FlipSign();
	}

	if (result.isZero()) result.sign = POSITIV;

	return result;
}


//-----------------------------------------------------------------------------
template <class type>
intLarge<type> intLarge<type>::faculty(const intLarge<type> &n_)
{
	intLarge<type> result = 1;

	for (intLarge<type> i = 2; i <= n_; i = i + (intLarge)1)
		result = result * i;
	return result;
}


//-----------------------------------------------------------------------------
template <class type>
intLarge<type> intLarge<type>::pow(intLarge<type> base, intLarge<type> exp)
{
	if (base.isZero() && exp.isZero())
		FatalError("0^0 is undefined\n");
	if (exp < 0)
		FatalError("exponent must be positiv\n");

	intLarge<type> result = 1;

	while (exp != 0)
	{
		if (getBit(exp.data[0],0) == 1)
			result *= base;
		exp >>= (type)1;
		base *= base;		
	}

	return result;
}


//-----------------------------------------------------------------------------
// very hacky root method, should be replaced by the Bakhshali method

template <class type>
intLarge<type> intLarge<type>::root(const intLarge<type> &z_)
{
	const intLarge<type> a = 3;

	if (z_ < 0)
		FatalError("root is a complex number\n");

	if (z_ == 0)
		return 1;

	intLarge<type> x[2] = { (intLarge)z_* (intLarge)100,(intLarge)z_ * (intLarge)10 };
	bool current = 1;

	do {
		x[!current] = (intLarge)x[current]
			- (pow(x[current], (intLarge)2) - (intLarge)z_ * (intLarge)100)
			/ ((intLarge)2 * x[current]);

		current = !current;

	} while (x[0] != x[1]);

	return x[current] / (intLarge)10;
}


//-----------------------------------------------------------------------------
template <class type>
type intLarge<type>::getSingleDigit(size_t p) const
{		
	auto q = p % typesize_bits;

	p -= q;
	p /= typesize_bits;

	return getBit(data[p], q);
}

//-----------------------------------------------------------------------------

template <class type>
void intLarge<type>::setBit(uint64_t p)
{
	auto q = p % typesize_bits;

	p -= q;
	p /= typesize_bits;

	if (data.size() < p+1)
		data.resize(p+1);

	data[p] = data[p] | ((type)1 << q);
}