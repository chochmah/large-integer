//-----------------------------------------------------------------------------
//
//	Explicit template instantiation for iint64_t
//
//-----------------------------------------------------------------------------

#include "LargeIntegerClass.h"


//-----------------------------------------------------------------------------
template <>
intLarge<uint64_t>& intLarge<uint64_t>::operator=(const int64_t val_)
{
	uint64_t val = std::abs(val_);
	sign = (val_ < 0) ? NEGATIV : POSITIV;
	
	data.resize(1);
	data[0] = val;	

	return *this;
}


//-----------------------------------------------------------------------------
template <>
intLarge<uint64_t>& intLarge<uint64_t>::operator=(const uint64_t val_)
{
	
	sign = POSITIV;

	data.resize(1);
	data[0] = val_;

	return *this;
}


//-----------------------------------------------------------------------------
template <>
intLarge<uint64_t> &intLarge<uint64_t>::AddAbsoluteOrdered(
															const intLarge<uint64_t> * small,
															const intLarge<uint64_t> * big,
															intLarge<uint64_t> &result)
{
	result.data.resize(big->data.size());

	unsigned char c = 0;
	for (size_t i = 0; i < small->data.size(); i++)
		c = _addcarry_u64(c, small->data[i], big->data[i], &result.data[i]);			

	for (size_t i = small->data.size(); i < big->data.size(); i++)
		c = _addcarry_u64(c, 0, big->data[i], &result.data[i]);

	if (c)
		result.data.push_back(1);

	return result;
}


//-----------------------------------------------------------------------------
template <>
intLarge<uint64_t> intLarge<uint64_t>::operator*(const intLarge<uint64_t> &val_) const
{
	intLarge<uint64_t> result(0);

	if ((this->data.size() + val_.data.size()) * typesize_bits > bitmax - 1)
	{
		std::cout << "intLarge<type> overflow\n";
		return result;
	}

	// cross multiply and sort into buckets
	uint64_t bucket_size = 1 + 2 * (this->data.size() + val_.data.size());

	std::vector<uint64_t> buckets	(bucket_size	,0);
	std::vector<uint64_t> overflow	(bucket_size + 1,0);

	for (uint64_t i = 0; i < this->data.size(); i++)
	{
		for (uint64_t l = 0; l < val_.data.size(); l++)
		{
			// take lower and upper 32 bits and multiply them seperatly to avoid overflow
			uint64_t a		= this->data[i];
			uint64_t b		= val_.data[l];

			uint32_t a_low	= ((a << 32) >> 32);
			uint32_t a_high = (a >> 32);
			uint32_t b_low	= ((b << 32) >> 32);
			uint32_t b_high	= (b >> 32);

			uint64_t l1		= (uint64_t)a_high	*	b_high;
			uint64_t l2		= (uint64_t)a_high	*	b_low;
			uint64_t l3		= (uint64_t)a_low	*	b_high;
			uint64_t l4		= (uint64_t)a_low	*	b_low;

			// add l1-l4 to suitable buckets
			overflow[i + l]		+= 
				_addcarry_u64(0, l4, buckets[i + l], &buckets[i + l]);
			overflow[i + l + 1] += 
				_addcarry_u64(0, l1 , buckets[i + l + 1], &buckets[i + l + 1]);
			overflow[i + l]		+= 
				_addcarry_u64(0, (uint64_t)((uint32_t)l3) << 32, buckets[i + l], &buckets[i + l]);
			overflow[i + l + 1] += 
				_addcarry_u64(0, (uint64_t)((uint32_t)((uint64_t)(l3>>32))), buckets[i + l+1], &buckets[i + l + 1]);
			overflow[i + l]		+= 
				_addcarry_u64(0, (uint64_t)((uint32_t)l2) << 32, buckets[i + l], &buckets[i + l]);
			overflow[i + l + 1] += 
				_addcarry_u64(0, (uint64_t)((uint32_t)((uint64_t)(l2 >> 32))), buckets[i + l + 1], &buckets[i + l + 1]);
		}
	}
	
	result.data.resize(bucket_size,0);

	unsigned char c = 0;
	c = _addcarry_u64(c, buckets[0], 0, &result.data[0]);
	overflow[1] += c;

	for (uint64_t i = 1; i < result.data.size(); i++)
	{
		unsigned char c = 0;
		c = _addcarry_u64(c, buckets[i], overflow[i-1], &result.data[i]);
		overflow[i + 1] += c;
	}

	result.RemoveLeadingZeros();
	result.sign = (result.isZero()) ? POSITIV : (this->sign == val_.sign) ? POSITIV : NEGATIV;

	return result;
}


//-----------------------------------------------------------------------------
// return position of highest set bit 

template<>
const uint64_t intLarge<uint64_t>::getFirstBitPosition() const
{
	uint64_t result = 0;

	auto m = this->data[this->data.size() - 1];
	
	for (int8_t i = 63; i >= 0; i--)
		if (1 == ((m >> i) & (uint64_t)1))
		{
			result = i;
			break;
		}

	result += (this->data.size() - 1)*typesize_bits;

	return result;
}


std::tuple<intLarge<uint64_t>, intLarge<uint64_t>>
DivisionSubroutine1(const intLarge<uint64_t> &A, const intLarge<uint64_t> &B);

//-----------------------------------------------------------------------------
std::tuple<intLarge<uint64_t>, intLarge<uint64_t>> DivisionSubroutine2(const intLarge<uint64_t> &A, const intLarge<uint64_t> &B)
{
	if (A >= B << 1)
	{		
		std::tuple<intLarge<uint64_t>, intLarge<uint64_t>> qr = DivisionSubroutine1(A - (B << 1), B);
		return std::make_tuple(std::get<0>(qr) + 2, std::get<1>(qr));
	}	

	uint64_t q = (A.getBinaryDigits()>1) ? A.getSingleDigit(A.getBinaryDigits()) * 2 + A.getSingleDigit(A.getBinaryDigits()-1) : 1;
	intLarge<uint64_t> T = B * q;

	for (int i = 0; i < 2; i++)
	{
		if (T > A)
		{
			q -= 1;
			T = T - B;
		}
	}

	return std::make_tuple(q, A - T);
}


//-----------------------------------------------------------------------------
std::tuple<intLarge<uint64_t>, intLarge<uint64_t>> 
	DivisionSubroutine1(const intLarge<uint64_t> &A, const intLarge<uint64_t> &B) 
{
	int64_t m = A.getBinaryDigits();
	int64_t n = B.getBinaryDigits();

	if (m < n)
		return std::make_tuple(0, A);

	if (m == n)
	{
		if (A < B) 
			return std::make_tuple(0, A);
		else
		{
			auto C = A - B;
			return std::make_tuple(1, C);
		}
	}

	if (m == n + 1)
		return DivisionSubroutine2(A, B);

	intLarge<uint64_t> *As = new  intLarge<uint64_t>(A);
	*As >>= m - n - 1;

	intLarge<uint64_t> *s = new intLarge<uint64_t>(A - (*As << (m - n - 1)));

	std::tuple<intLarge<uint64_t>, intLarge<uint64_t>>  *strich 
		= new std::tuple<intLarge<uint64_t>, intLarge<uint64_t>>(DivisionSubroutine2(*As, B));
	
	delete As;	
	
	std::tuple<intLarge<uint64_t>, intLarge<uint64_t>> *qr
		= new std::tuple<intLarge<uint64_t>, intLarge<uint64_t>>(DivisionSubroutine1((std::get<1>(*strich) << (m-n-1)) + *s, B));
	
	delete s;

	auto result = std::make_tuple((std::get<0>(*strich) << (m-n-1)) + std::get<0>(*qr), std::get<1>(*qr));

	delete qr;
	delete strich;

	return result;
}


//-----------------------------------------------------------------------------
std::tuple<intLarge<uint64_t>, intLarge<uint64_t>>
BarrettsMethod(const intLarge<uint64_t> &A_, const intLarge<uint64_t> &B_)
{
	int64_t m = A_.getBinaryDigits();
	int64_t n = B_.getBinaryDigits();

	intLarge<uint64_t> A = A_ >> (n - 1);
	intLarge<uint64_t> Q = A_ >> (m - n + 1);
	intLarge<uint64_t> R = A - B_*Q;

	while (R >= 0 && R <= B_)
	{
		if (R < 0)
		{
			R = R + B_;
			Q--;
		}
		else
		{
			R = R - B_;
			Q++;
		}
	}

	return std::make_tuple(Q,R);
}

/*
//-----------------------------------------------------------------------------
template <>
std::tuple<intLarge<uint64_t>, intLarge<uint64_t>> intLarge<uint64_t>::div(const intLarge<uint64_t>& val_) const
{
	intLarge<uint64_t> val = abs(val_);
	intLarge<uint64_t> tis = abs(*this);

	std::tuple<intLarge<uint64_t>, intLarge<uint64_t>> m = BarrettsMethod(tis, val);

	std::get<0>(m).sign = (this->sign == val_.sign) ? POSITIV : NEGATIV;	
	std::get<1>(m).sign = (std::get<0>(m) == 0) ? POSITIV : this->sign;

	if (std::get<0>(m)==0) std::get<1>(m).sign = this->sign;
	
	return m;
}
*/

//-----------------------------------------------------------------------------

template <>
std::tuple<intLarge<uint64_t>, intLarge<uint64_t>> intLarge<uint64_t>::div(const intLarge<uint64_t> & val_) const
{
	if (val_.isZero())
		FatalError("division by zero");

	auto sign = !(!val_.sign ^ !this->sign);

	intLarge<uint64_t> val = abs(val_);
	intLarge<uint64_t> tis = abs(*this);

	if (val > tis)
	{
		auto result = std::make_tuple(0, *this);
		return result;
	}

	int64_t firstbit_divisor	= val_.getFirstBitPosition();
	int64_t firstbit_divident	 = tis.getFirstBitPosition();

	int64_t n = firstbit_divisor;
	int64_t m = firstbit_divident;

	int64_t lower_bound_bit = MAX(m - n - 0, 0);

	intLarge<uint64_t> middle(tis);

	uint64_t counter = 0;
	int64_t counterD = 0;

	intLarge<uint64_t> result(0);
	intLarge<uint64_t> M, T;

	M = val << lower_bound_bit + 1;

	do {
		if (middle.getFirstBitPosition() == firstbit_divident - counter)
		{
			M >>= 1;

			if (middle >= M)
			{
				middle -= M;
				result.setBit(lower_bound_bit-counterD);
			}
			else
			{
				counter--;
			}
			counterD++;
		}

		counter++;
	} while (counterD <= lower_bound_bit);	
	
	if (result.data.size() == 0) result.data.push_back(false);
	result.sign = (this->sign == val_.sign) ? POSITIV : NEGATIV;
	middle.sign = (middle == 0) ? POSITIV : this->sign;

	return std::make_tuple(result, middle);
}



/*
//-----------------------------------------------------------------------------
template <>
std::tuple<intLarge<uint64_t>, intLarge<uint64_t>> intLarge<uint64_t>::div(const intLarge<uint64_t> & val_) const
{
	if (val_.isZero())
		FatalError("division by zero");

	auto sign = !(!val_.sign ^ !this->sign);

	intLarge<uint64_t> val		= abs(val_);
	intLarge<uint64_t> tis		= abs(*this);

	if (val > tis) 
	{
		auto result = std::make_tuple(0,*this);
		return result;
	}

	int64_t firstbit_divisor	= val_.getFirstBitPosition();
	int64_t firstbit_divident	= tis.getFirstBitPosition();

	int64_t lower_bound_bit	= MAX(firstbit_divident - 1 - firstbit_divisor,0);
	int64_t upper_bound_bit	= MAX(firstbit_divident + 1 - firstbit_divisor,0);

	intLarge<uint64_t> upper_bound = 1;	
	upper_bound <<= upper_bound_bit;
	
	intLarge<uint64_t> lower_bound = 1;
	lower_bound <<= lower_bound_bit;


	intLarge<uint64_t> middle, estimate;

	do {		
		middle = (upper_bound + lower_bound) >>= (uint64_t)1;

		estimate = middle * val;

		if (estimate < tis)
			lower_bound		= middle;
		else
			upper_bound		= middle;

	} while ((estimate > tis) ? (estimate - tis) >= val : (tis - estimate) >= val);

	if (middle * val > tis) middle -= (intLarge<uint64_t>)1;	

	auto remainder = tis - abs(middle * val);

	if (middle.data.size() == 0) middle.data.push_back(false);
	middle.sign = (this->sign == val_.sign) ? POSITIV : NEGATIV;
	remainder.sign = (remainder==0) ? POSITIV : this->sign;

	auto result = std::make_tuple(middle, remainder);
	return result;
}
*/

//-----------------------------------------------------------------------------
template <>
std::vector<int8_t> intLarge<uint64_t>::AbsoluteConvertToVectorDigits() const
{
	std::vector<int8_t> dec;
	std::vector<int8_t> pot;

	for (uint64_t i = 0; i < this->data.size(); i++)
	{
		for (uint64_t u = 0; u < typesize_bits; u++)
		{
			if (i == 0 && u == 0)
				pot.push_back(1);
			else
			{
				//double pot
				int8_t overflow{ 0 };

				for (uint64_t k = 0; k < pot.size(); k++)
				{
					pot[k] *= 2;
					pot[k] += overflow;
					if (pot[k] > 9)
					{
						overflow = (pot[k] - (pot[k] % 10)) / 10;
						pot[k] = (pot[k] % 10);
					}
					else
						overflow = 0;
				}
				if (overflow)
					pot.push_back(overflow);
			}


			//std::cout << i << "	" << u << "	" << (GETBIT(this->data[i], u)) << "\n";
			if (getBit(this->data[i],u))
			{
				// add current pot to number dec
				int8_t overflow{ 0 };

				if (dec.size() < pot.size()) dec.resize(pot.size());

				for (uint64_t k = 0; k < pot.size(); k++)
				{
					dec[k] += pot[k] + overflow;
					if (dec[k] > 9)
					{
						overflow = (dec[k] - (dec[k] % 10)) / 10;
						dec[k] = (dec[k] % 10);
					}
					else
						overflow = 0;
				}
				if (overflow)
					dec.push_back(overflow);
			}
		}
	}

	return dec;
}


//-----------------------------------------------------------------------------
template<>
void intLarge<uint64_t>::flipBits(intLarge<uint64_t> &val_)
{
	for (auto &o : val_.data)
		o = ~o;
}


//-----------------------------------------------------------------------------
template <>
bool intLarge<uint64_t>::getBit(const uint64_t _var, const uint64_t _p)
{
	return (1==((_var & ((uint64_t)1 << _p)) >> _p));
}


//-----------------------------------------------------------------------------
// absolutely brilliant random-method, where is my fields-medal? 

template <>
intLarge<uint64_t> intLarge<uint64_t>::rand(const intLarge<uint64_t> &n_)
{
	if (n_ < 1) return intLarge(0);

	intLarge<uint64_t> result;
	result.data.resize(n_.data.size());

	int64_t first = n_.data.size() - 1;
	
	for (int64_t i = 0; i <  first; i++)
	{
		result.data[i]		= std::rand();
		result.data[i]		<<= 32;
		result.data[i]		+= std::rand();
	}

	if (n_.data[first] < UINT32_MAX)
	{
		result.data[first]	= 0;
		result.data[first]	= std::rand() % n_.data[first];
	}
	else {
		result.data[first]	= std::rand() % (n_.data[first] >> 32);
		result.data[first]	<<= 32;
		result.data[first]	+= std::rand();
	}

	return result;
}



//-----------------------------------------------------------------------------
template <>
intLarge<uint64_t>& intLarge<uint64_t>::operator++()
{
	for (int i = 0; i < data.size(); i++)
	{
		uint64_t *p = &data[i];

		if (*p < UINT64_MAX)
		{
			for (int l = 0; l < typesize_bits; l++)
			{
				uint64_t mask = (uint64_t)1 << l;

				if (!(*p & mask))
				{
					*p |= mask;
					return *this;
				}
				else
					*p &= ~mask;
			}
		}
		else
			*p = 0;
	}

	this->addDigit(1);

	return *this;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
template class intLarge<uint64_t>;
