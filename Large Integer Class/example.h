#pragma once


namespace exampleSpace
{
	//-----------------------------------------------------------------------------
	template<typename intL>
	void Example()
	{
		// assignment 

		intL	a("-123456789");
		intL	x{ 123 };
		intL	y = 236;
		intL	z = "987654321";
		int64_t		i = (int64_t)x;

		// arithmetic

		x += y - z / y * (intL)3;
		z = intL::pow(z, 3);
		a = intL::faculty(7);
		y = intL::root(z);
		a = intL::rand(a);
		x = intL::abs(x);

		// bit arithmetic

		y *= a % y;
		x = z << 2;

		// comparison

		if (x > z)
			x *= (intL)2;

		// output methods

		auto m = intL::getDisplayDigits();

		std::cout << "x  = " << x << "\n which in binary is ";
		x.printBinary();

		intL::setDisplayDigits(100);

		std::cout << "x  with more digits: " << x << "\n\n";

		intL::setDisplayDigits(m);
	}
}